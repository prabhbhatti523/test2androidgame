package com.example.tappyspaceship01;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.util.Random;

public class GameEngine extends SurfaceView implements Runnable {

    // Android debug variables
    final static String TAG="DINO-RAINBOWS";



    // screen size
    int screenHeight;
    int screenWidth;

    // game state
    boolean gameIsRunning;

    // threading
    Thread gameThread;


    // drawing variables
    SurfaceHolder holder;
    Canvas canvas;
    Paint paintbrush;

    int score =0;
    int lives = 3;

    Player player;
    Item item1;
    Item item2;
    Item item3;

    String personTapped="";


    int xpos;
    int ypos;


    // -----------------------------------
    // GAME SPECIFIC VARIABLES
    // -----------------------------------

    // ----------------------------
    // ## SPRITES
    // ----------------------------

    // represent the TOP LEFT CORNER OF THE GRAPHIC

    // ----------------------------
    // ## GAME STATS
    // ----------------------------


    public GameEngine(Context context, int w, int h) {
        super(context);

        this.holder = this.getHolder();
        this.paintbrush = new Paint();

        this.screenWidth = w;
        this.screenHeight = h;

        this.printScreenInfo();

        // put the initial starting position of your player and enemies
        this.player = new Player(getContext(), 1550, 200);

        this.item1 = new Item(getContext(), 150, 0);
        this.item2 = new Item(getContext(), 150, 200);
        this.item3 = new Item(getContext(), 150, 400);
        this.item2.setImage(BitmapFactory.decodeResource(context.getResources(), R.drawable.candy64));
        this.item3 = new Item(getContext(), 150, 400);
        this.item3.setImage(BitmapFactory.decodeResource(context.getResources(), R.drawable.poop64));


    }



    private void printScreenInfo() {

        Log.d(TAG, "Screen (w, h) = " + this.screenWidth + "," + this.screenHeight);
    }

    private void spawnPlayer() {
        //@TODO: Start the player at the left side of screen
    }
    private void spawnEnemyShips() {
        Random random = new Random();

        //@TODO: Place the enemies in a random location

    }

    // ------------------------------
    // GAME STATE FUNCTIONS (run, stop, start)
    // ------------------------------
    @Override
    public void run() {
        while (gameIsRunning == true) {
            this.updatePositions();
            this.redrawSprites();
            this.setFPS();
        }
    }


    public void pauseGame() {
        gameIsRunning = false;
        try {
            gameThread.join();
        } catch (InterruptedException e) {
            // Error
        }
    }

    public void startGame() {
        gameIsRunning = true;
        gameThread = new Thread(this);
        gameThread.start();
    }


    // ------------------------------
    // GAME ENGINE FUNCTIONS
    // - update, draw, setFPS
    // ------------------------------

    public int[] random(){
        Random r = new Random();
        int line1 =1;


        line1 = r.nextInt(5 - 1) + 1;
        if(line1 ==1){
            xpos = 150;
            ypos = 0;

        }
        else if(line1 ==2){
            xpos = 150;
            ypos = 200;

        }
        else if(line1 ==3 ){
            xpos = 150;
            ypos = 400;
        }
        else{
            xpos = 150;
            ypos = 600;
        }
        int[] intArray = new int[]{ xpos, ypos};

        return intArray;
        
    }

    public void updatePositions() {


        // set the speed of the items
        item1.setxPosition(item1.getxPosition()+25);
        item2.setxPosition(item2.getxPosition()+15);
        item3.setxPosition(item3.getxPosition()+45);

        // update hitbox
        player.updateHitbox();
        item1.updateHitbox();
        item2.updateHitbox();
        item3.updateHitbox();

        int itemYPos =150;
        int itemXPos =0;

        Random r = new Random();
        int line =1;


        if(player.getHitbox().intersect(item1.getHitbox())== true){

            // generate random number so the the item appears in a random line
            line = r.nextInt(5 - 1) + 1;
            if(line ==1){
                itemXPos = 150;
                itemYPos = 0;

            }
            else if(line ==2){
                itemXPos = 150;
                itemYPos = 200;

            }
            else if(line ==3 ){
                itemXPos = 150;
                itemYPos = 400;
            }
            else{
                itemXPos = 150;
                itemYPos = 600;
            }


            Log.d(TAG+"random","Random"+line);


            lives = lives+1;
            score = score+1;
            canvas.drawText("LIVES = " + lives, 600, 50, paintbrush);
            canvas.drawText("SCORE = " + score, 1000, 50, paintbrush);
            item1.setxPosition(itemXPos);
            item1.setyPosition(itemYPos);

            player.updateHitbox();
            item1.updateHitbox();
            item2.updateHitbox();


        }
        if(player.getHitbox().intersect(item2.getHitbox())== true){
            // generate random number so the the item appears in a random line
            line = r.nextInt(4 - 1) + 1;
            if(line ==1){
                itemXPos = 150;
                itemYPos = 0;

            }
            else if(line ==2){
                itemXPos = 150;
                itemYPos = 200;

            }
            else if(line ==3 ){
                itemXPos = 150;
                itemYPos = 400;
            }
            else{
                itemXPos = 150;
                itemYPos = 600;
            }
            lives = lives+1;
            score = score+1;
            canvas.drawText("LIVES = " + lives, 600, 50, paintbrush);
            canvas.drawText("SCORE = " + score, 1000, 50, paintbrush);
            item2.setxPosition(itemXPos);
            item2.setyPosition(itemYPos);

            player.updateHitbox();
            item1.updateHitbox();
            item2.updateHitbox();

        }
        if(player.getHitbox().intersect(item3.getHitbox()) ==  true){

            // generate random number so the the item appears in a random line
            line = r.nextInt(4 - 1) + 1;
            if(line ==1){
                itemXPos = 150;
                itemYPos = 0;

            }
            else if(line ==2){
                itemXPos = 150;
                itemYPos = 200;

            }
            else if(line ==3 ){
                itemXPos = 150;
                itemYPos = 400;
            }
            else{
                itemXPos = 150;
                itemYPos = 600;
            }



            lives = lives-1;
            score = score-1;
            canvas.drawText("LIVES = " + lives, 600, 50, paintbrush);
            canvas.drawText("SCORE = " + score, 1000, 50, paintbrush);
            item3.setxPosition(itemXPos);
            item3.setyPosition(itemYPos);

            player.updateHitbox();
            item3.updateHitbox();


        }







        if(item1.getxPosition() >= this.screenWidth){
            this.item1.setxPosition(150);

        }
        if(item2.getxPosition() >= this.screenWidth){
            this.item2.setxPosition(150);

        }

        if(item3.getxPosition() >= this.screenWidth){

            this.item3.setxPosition(150);

        }




        player.updateHitbox();
        item1.updateHitbox();
        item2.updateHitbox();
        item3.updateHitbox();


    }

    public void redrawSprites() {
        if (this.holder.getSurface().isValid()) {
            this.canvas = this.holder.lockCanvas();

            //----------------

            // configure the drawing tools
            this.canvas.drawColor(Color.argb(255,255,255,255));
            paintbrush.setColor(Color.WHITE);

            // draw player
            // draw player graphic on screen
            canvas.drawBitmap(player.getImage(), player.getxPosition(), player.getyPosition(), paintbrush);
            // draw the player's hitbox
            canvas.drawRect(player.getHitbox(), paintbrush);


            // draw item
            // draw item graphic on screen
            canvas.drawBitmap(item1.getImage(), item1.getxPosition(), item1.getyPosition(), paintbrush);
            // draw the item's hitbox
            canvas.drawRect(item1.getHitbox(), paintbrush);

            // draw item graphic on screen
            canvas.drawBitmap(item2.getImage(), item2.getxPosition(), item2.getyPosition(), paintbrush);
            // draw the item's hitbox
            canvas.drawRect(item2.getHitbox(), paintbrush);

            canvas.drawBitmap(item3.getImage(), item3.getxPosition(), item3.getyPosition(), paintbrush);
            // draw the item's hitbox
            canvas.drawRect(item3.getHitbox(), paintbrush);

            // DRAW THE PLAYER HITBOX

            // ------------------------
            // 1. change the paintbrush settings so we can see the hitbox
            paintbrush.setColor(Color.BLACK);

            paintbrush.setStyle(Paint.Style.STROKE);
            paintbrush.setStrokeWidth(4);

            // draw lanes in rectangle form

            canvas.drawRect(this.player.getHitbox(), paintbrush);
            this.canvas.drawRect(100,200,1500,220,paintbrush);
            this.canvas.drawRect(100,400,1500,420,paintbrush);
            this.canvas.drawRect(100,600,1500,620,paintbrush);
            this.canvas.drawRect(100,800,1500,820,paintbrush);

           // this.canvas.drawLine(100,200,1500,200,paintbrush);
//            this.canvas.drawLine(100,400,1500,400,paintbrush);
//            this.canvas.drawLine(100,600,1500,600,paintbrush);
//            this.canvas.drawLine(100,800,1500,800,paintbrush);
            paintbrush.setStrokeWidth(4);
            paintbrush.setTextSize(50);
            canvas.drawText("LIVES = "+lives, 600, 50, paintbrush);
            canvas.drawText("SCORE = "+score, 1000, 50, paintbrush);

            // player hitbox
            canvas.drawRect(this.player.getHitbox(), paintbrush);

            //----------------
            this.holder.unlockCanvasAndPost(canvas);
        }
    }

    public void setFPS() {
        try {
            gameThread.sleep(120);
        }
        catch (Exception e) {

        }
    }

    // ------------------------------
    // USER INPUT FUNCTIONS
    // ------------------------------


    String fingerAction = "";

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int userAction = event.getActionMasked();
        //@TODO: What should happen when person touches the screen?
        if (userAction == MotionEvent.ACTION_DOWN) {
            if(userAction == MotionEvent.ACTION_DOWN){

                float fingerXPosition = event.getX();
                float fingerYPosition = event.getY();

                int middle = this.screenHeight / 2;
                if (fingerYPosition >= middle) {
                 //   personTapped = "upperHalf";
                    if(player.getyPosition() == 600){
                    //    personTapped = "";
                    }
                    else
                    {
                        player.setyPosition(player.getyPosition() + 200);
                        player.updateHitbox();
                    }
                }
                else if (fingerYPosition < middle) {
                 //   personTapped = "lowerHalf";
                    if(player.getyPosition() == 0){
                     //   personTapped = "";
                    }
                    else
                    {
                        player.setyPosition(player.getyPosition() - 200);
                        player.updateHitbox();
                    }

                }

            }

        }
        else if (userAction == MotionEvent.ACTION_UP) {

        }

        return true;
    }
}
